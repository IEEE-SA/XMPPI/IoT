Authentication
========================

All entities are authenticated before access is granted to the underlying XMPP network. The process of authentication is based on the
Simple Authentication and Security Layer protocol (SASL) and is defined in [RFC 6120](https://tools.ietf.org/html/rfc6120).